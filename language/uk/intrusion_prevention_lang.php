<?php

$lang['intrusion_prevention_app_description'] = 'Додаток «Система запобігання вторгненням» активно відстежує мережевий трафік і блокує небажаний трафік, перш ніж він може зашкодити вашій мережі.';
$lang['intrusion_prevention_app_name'] = 'Система запобігання вторгненням';
$lang['intrusion_prevention_block_time'] = 'Час блокування';
$lang['intrusion_prevention_blocked_list'] = 'Список заблокованих';
$lang['intrusion_prevention_checksum_invalid'] = 'Контрольна сума недійсна.';
$lang['intrusion_prevention_security_id'] = 'ID безпеки';
$lang['intrusion_prevention_white_list'] = 'Білий список';
$lang['intrusion_prevention_details'] = 'Деталі';
$lang['intrusion_prevention_source_address'] = 'Адреса джерела';
$lang['intrusion_prevention_reference_warning'] = 'Не всі зовнішні посилання були перевірені. Деякі посилання можуть бути недоступні.';
